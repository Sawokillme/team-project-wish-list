$('.message').mouseover(function(){
    
   $('#messageBox').hide('slow'); 
    
});



function checkInformationEntered(){
    var name = document.getElementById('Reg-username').value;
    var email = document.getElementById('Reg-email').value;
    var password = document.getElementById('Reg-password').value;
    var repassword = document.getElementById('Reg-repassword').value;
    
    var emailValidatePattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    
    var errorField = document.getElementById('errorBox');

    if(password.length<7){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Password at Least 6 characters');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(password!=repassword){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Password and Confirm Password Not Same');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(name.length=00){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Please Enter Name');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(email.length=00){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Please Enter email');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(!emailValidatePattern.test(email)){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Please Enter an valid email address');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else{
        errorField.innerHTML='';
        document.getElementById('signUpAsDonor').submit();
    }
}




function checkAddItem(){
    var first = document.getElementById('1stCaregory').value;
    var second = document.getElementById('2ndCaregory').value;
    var third = document.getElementById('3rdCaregory').value;
    var quantity = document.getElementById('quantity').value;
      
    var errorField = document.getElementById('errorBox');

    if(first==''){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Item Is Not Selected');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(second==''){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Item Is Not Selected');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(third==''){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Item Is Not Selected');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(quantity==0||quantity==''){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Quantity Is Not Entered');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else{
        errorField.innerHTML='';
        document.getElementById('addItem').submit();
    }
}



function checkUpdateItem(){
    var first = document.getElementById('1stCaregoryupdate').value;
    var second = document.getElementById('2ndCaregoryupdate').value;
    var third = document.getElementById('3rdCaregoryupdate').value;
    var quantity = document.getElementById('quantityupdate').value;
      
    var errorField = document.getElementById('errorBox');

    if(first==''){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Item Is Not Selected');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(second==''){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Item Is Not Selected');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(third==''){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Item Is Not Selected');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else if(quantity==0||quantity==''){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Quantity Is Not Entered');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else{
        errorField.innerHTML='';
        var getThirdCategory = document.getElementById('3rdCaregoryupdate').value;
        var quantityUpdate =  document.getElementById("quantityupdate").value;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "update.php?itemName="+getThirdCategory+"&quantity="+quantityUpdate+"&original="+originalItem, true);
        xmlhttp.send();
        document.getElementById('updateItem').submit();
    }
}




function checkFieldAndDatabase(){
    var email = document.getElementById('emailAddress').value;
      
    var errorField = document.getElementById('errorBox');
    var emailValidatePattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    
    if(email==''||!emailValidatePattern.test(email)){
        errorField.innerHTML='';
        var message = document.createElement('p');
        var messageText = document.createTextNode('Please enter an valid email');
        message.appendChild(messageText);
        errorField.appendChild(message);
    }
    else{        
        var xmlhttp = new XMLHttpRequest();
        
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("errorBox").innerHTML= xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "retrievePassword.php?email="+email, true);
        xmlhttp.send();
        //document.getElementById('updateItem').submit();                
    }
}