<!DOCTYPE html>
<?php

session_start();
include('SystemUser.php');
if(isset($_SESSION['user'])){
    $guestUserID=$_SESSION['user'];
}
$systemUserObject = new SystemUser();
$_SESSION['systemUser'] = $systemUserObject;

if(isset($_SESSION['type']))
$type=$_SESSION['type'];

include("db_connect.php");

$visitingUserID=$_GET['name'];
echo $visitingUserID;
?>
<html>
<head>
    <title>Wish List page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">    
    <link href = "css/homestyle.css" rel="stylesheet">  
    <link href = "css/wishListStyle.css" rel="stylesheet">     
    <link href = "css/wishListComponent.css" rel="stylesheet">  
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">    
    <script language="javascript" type="text/javascript" src="js/jquery-1.11.3.js"></script>
    <script language="javascript" type="text/javascript" src="js/bootstrap.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    
    <script src="https://maps.googleapis.com/maps/api/js"></script>

    
</head>
<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
    
    <div class="headers">
        <div class="header-bar">
            <h1>Cybercare Wish List</h1>
            <ul>                
                <?php
                
                if(isset($_SESSION['user'])){
                    $logout='<li><a href="logout.php"><img src="images/profile.png" alt="profile"/></a></li>';
                    echo $logout;
                }
                
                ?>                 
                <li><a data-toggle="modal" data-target="#searchItem"><img src="images/search.png" alt="navigation"/></a></li>     
                
                <?php
                
                if(isset($_SESSION['user'])){
                    if($type=='orphanage')
                        $homeForUser='<li><a href="wishList(orphanage).php"><img src="images/home.png" alt="home page"/></a></li> ';
                    else
                        $homeForUser='<li><a href="donorHomePage.php"><img src="images/home.png" alt="home page"/></a></li> ';
                    echo $homeForUser;
                }
                else{
                    echo '<li><a href="home.php"><img src="images/home.png" alt="home page"/></a></li>';
                }
                
                ?>
                   
            </ul>
        </div>
    </div>
    
    
    <div class="wishListContainer">
        <div class="profile">
            <div class="sidebar">
                <img class="profilepicture" 
                     <?php
                        $pic='';                                             
                        $sql='SELECT Profile_picture FROM account WHERE Account_id="'.$visitingUserID.'" LIMIT 1;';
                        $stmt=$mysqli->query($sql);
                        while($row=$stmt->fetch_assoc()){
                            $pic=$row['Profile_picture'];
                        }
                        echo 'src="'.$pic.'"';
                                                                         ?>>
                <ul>
                    <?php
                        if(isset($guestUserID)){
                            $OaccountID=array();
                            $selfCheck = false;
                            $result = false;
                            $sql = 'SELECT * FROM followedlist WHERE Account_ID="'.$guestUserID.'";';
                            $stmt = $mysqli->query($sql);
                            while($row=$stmt->fetch_assoc()){
                                $OaccountID[] = $row['OAccount_id'];
                            }
                            
                            if($guestUserID==$visitingUserID){
                                $selfCheck = true;
                            }
                            
                            if(!$selfCheck){
                                foreach($OaccountID as $value){
                                    if($visitingUserID==$value){
                                        $result = true;
                                    }                                
                                }
                                if(!$result)
                                    echo '<li><a href="follow.php?personToFollow='.$visitingUserID.'">Follow</a></li>';
                                else
                                    echo '<li><a href="unfollow.php?personToUnfollow='.$visitingUserID.'">Unfollow</a></li>';
                            }
                        }
                    ?>
                    <li><span>Share Link</span><div class="fb-share-button" data-href="preview.php?name=<?php echo $visitingUserID ?>" data-layout="button_count"></div></li>
                </ul>
            </div>
            
            <div class="profileAndWishList">
                <div class="profilePanel">
                    <table>
                        <caption>Profile</caption>
                        
                        <?php 
                            $name='';
                            $contact_no='';
                            $email='';
                            $output='';
                            $person_in_charge='';
                            $numberOfKids='';
                            $address = '';
                            $sql='SELECT * FROM account WHERE Account_id="'.$visitingUserID.'" LIMIT 1;';
                            $stmt=$mysqli->query($sql);
                            while($row=$stmt->fetch_assoc()){
                                $name=$row['Name'];
                                $contact_no=$row['Contact_no'];
                                $email=$row['email'];
                            }        
                            $sql='SELECT * FROM orphanage WHERE OAccount_id="'.$visitingUserID.'" LIMIT 1;';
                            $stmt=$mysqli->query($sql);
                            while($row=$stmt->fetch_assoc()){
                                $person_in_charge=$row['Person_in_charge'];
                                $numberOfKids=$row['Number_of_kids'];
                                $address=$row['address1'].' '.$row['address2'].' '.$row['PostCode'].' '.$row['state'];
                            }        
                            
$output =<<<HTML
<tbody>
<tr>
<td class="fieldName">Name</td>
<td colspan="3" class="outputName">{$name}</td>
</tr>
<tr>
<td class="fieldName">Address</td>
<td colspan="3" class="outputName" id="address" value="{$address}">{$address}</td>
</tr>
<tr>
<td class="fieldName">Person in charge</td>
<td class="outputName">{$person_in_charge}</td>
<td class="fieldName">Contact Number</td>
<td class="outputName">{$contact_no}</td>
</tr>
<tr>
<td class="fieldName">Number Of Kids</td>
<td class="outputName" colspan="4">{$numberOfKids}</td>
</tr>
<tr>
</tr>
</tbody>                                    
HTML;

echo $output;
                        ?>
                    </table>
                </div>
                
                
                <div id="map-canvas"></div>
                
                <div class="wishList">
                    <table> 
                        <caption>Wish List</caption>
                        <thead>
                        <tr>
                            <td class="tableHead">Category</td>
                            <td class="tableHead">SubCategory</td>
                            <td class="tableHead">Item</td>
                            <td class="tableHead">Qtn.</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $itemMainCate='';
                            $itemSubCate='';
                            $itemName='';
                            $output='';
                            $result=null;
                            $sql='';
                            $stmt = null;
                            $sql2='';
                            $stmt2=null;
                            $itemID='';

                            $sql = 'SELECT * FROM addeditem WHERE OAccount_id="'.$visitingUserID.'";';
                            $stmt = $mysqli->query($sql);
                            while($row=$stmt->fetch_assoc()){
                                $itemID=$row['item_id'];
                                $sql2 = 'SELECT * FROM item WHERE item_id="'.$row['item_id'].'";';
                                $stmt2 = $mysqli->query($sql2);
                                while($result=$stmt2->fetch_assoc()){
                                    $itemMainCate = $result['item_main_category'];
                                    $itemSubCate = $result['item_sub_category'];
                                    $itemName = $result['item_type'];
                                }
                                $storedQuantity=$row['quantity'];
                                $values = array("Main"=>$itemMainCate,
                                                "Sub"=>$itemSubCate,
                                                "item"=>$itemName,
                                                "Quantity"=>$storedQuantity);
                                $output.=<<<HTML
<tr id="{$itemID}">
<td class="firstCate">{$itemMainCate}</td>
<td class="secondCate">{$itemSubCate}</td>
<td class="thirdCate">{$itemName}</td>
<td class="quantity">{$storedQuantity}</td>
HTML;
                            }
                            echo $output;
                            ?> 
                        </tbody>
                    </table>
                </div>
            </div>    
            
                        <?php
include('search.php');
?>

        <script>
            var geocoder=0;
            var map;
            function initialize() {
              geocoder = new google.maps.Geocoder();
              var latlng = new google.maps.LatLng(codeAddress());
              var mapOptions = {
              zoom: 16,
              //center: latlng,
                mapTypeId:google.maps.MapTypeId.ROADMAP
              }
              map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            }

            function codeAddress() {
              var address = document.getElementById('address').innerText;
              geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  map.setCenter(results[0].geometry.location);
                  var marker = new google.maps.Marker({
                      map: map,
                      position: results[0].geometry.location
                  });
                } else {
                  alert('Geocode was not successful for the following reason: ' + status);
                }
              }); 
            console.log(address);
            }
            console.log(geocoder);
            google.maps.event.addDomListener(window, 'load', initialize);

    </script>
                        
            
            
</body>