<?php

session_start();

include("db_connect.php");


include("user.php");

if(isset($_SESSION['user'])){
    if($_SESSION['type']=='donor'){
        header('location:donorHomePage.php');
    }
    else
        header('location:wishList(orphanage).php');
}


$sql = '';
$row = null;
$stmt = null;
$publicUser = new user();

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['DonorReg']))
        $publicUser->DonorRegister($mysqli);
    elseif(isset($_POST['OrpReg']))
        $publicUser->OrphanageRegister($mysqli);
    elseif(isset($_POST['loginToSystem']))
        $publicUser->LoginToSystem($mysqli);
    elseif(isset($_POST['loginToSystemDonor']))
        $publicUser->LoginToSystemAsDonor($mysqli);
    elseif(isset($_POST['searching'])){
        $input = $_POST['searchInputKeyWord'];
        header("location:search.php?searchInput=$input");
    }
}

include_once 'header.php';

?>
<?php if(isset($_SESSION['user'])){
    echo '<div class="banners"><div class="adSection"><h1 class="ad">Welcome</h1>';
    echo '<h2 class="firstQuote">Click on the house above to start</h2>';
    echo '<h2 class="firstQuote">Or view who to donate below</h2>';
    echo '</div></div>';
}
else {
            $Html = <<<HTML
<div class="banners">
<div class="adSection">
<h1 class="ad">Sign Up Now For FREE!</h1>
<h2 class="firstQuote"><em>&quot;one has never become poor by giving.&quot;</em></h2>
<h2 class="author"><em>-Anne Frank, diary of Anne Frank</em></h2>
<a class="buttonLink" data-toggle="modal" data-target="#signup">Sign Up</a>
<a class="buttonLink" data-toggle="modal" data-target="#login">Sign In</a>
</div>
</div>           
HTML;
echo $Html;
}


?>

    
    <div class="miniTitleBar">
        <h1>Who to donate?</h1>
        <h2>Donate To Them</h2>
    </div>
    
<?php 
    $tempItemMainCategory ='';
    $tempItemSubCategory='';
    $tempItemName ='';
    $quantity = '';
    $tempAccountName = array();
    $tempAccountID = array();
    $pic=array();
    $content = '';
    $content2 = '';
    $sql = 'SELECT Account_id, Name, Profile_picture FROM account WHERE Type_of_user ="orphanage" ORDER BY RAND() LIMIT 0,5;';
    $stmt = $mysqli->query($sql);
    while($row=$stmt->fetch_assoc()){
        $tempAccountID[] = $row['Account_id'];
        $tempAccountName[] = $row['Name'];
        $pic[]=$row['Profile_picture'];
    }    
    if($stmt->num_rows>=3){
    for($index=0;$index<3;$index++){
        $content2='';
        $sql = 'SELECT item_id , quantity FROM addeditem WHERE OAccount_id="'.$tempAccountID[$index].'" LIMIT 3;';
        $stmt = $mysqli->query($sql);        
        if($stmt->num_rows>0){
        while($row=$stmt->fetch_assoc()){
            $quantity = $row['quantity'];
            $sql2 = 'SELECT * FROM item WHERE item_id="'.$row['item_id'].'";';
            $stmt2 = $mysqli->query($sql2);
                while($row2=$stmt2->fetch_assoc()){
                    $tempItemMainCategory = $row2['item_main_category'];
                    $tempItemSubCategory = $row2['item_sub_category'];
                    $tempItemName = $row2['item_type'];
                    $content2 .=<<<HTML
<tr>
<td class="firstCate">{$tempItemMainCategory}</td>
<td class="secondCate">{$tempItemSubCategory}</td>
<td class="thirdCate">{$tempItemName}</td>
<td class="quantity">{$quantity}</td>
</tr>               
HTML;
                }
            }
         $content .=<<<HTML
<div class="context-container">
<div class="Incontainer">
<div class="context">                
<div class="briefInformation">
<table> 
<caption><a href="preview.php?name={$tempAccountID[$index]}">{$tempAccountName[$index]}</a></caption>
<thead>
<tr>
<td class="tableHead">Category</td>
<td class="tableHead">SubCategory</td>
<td class="tableHead">Item</td>
<td class="tableHead">Qtn.</td>
</tr>
</thead>
<tbody>
{$content2}
</tbody>
<tfoot>
<tr>
<td colspan="4"><a class="toWishList" href="preview.php?name={$tempAccountID[$index]}">See More</a></td>
</tr>
</tfoot>
</table>
</div>
<img class="profile-picture" src="{$pic[$index]}">                 
</div>        
</div>
</div>
HTML;
        }
    
           else
                continue;
    }
echo $content;
    }
else
    echo '<div class="msg-panel"><h1 class="errorMsg"> no Orphanage request Item yet </h1></div>';
?>
    
  
    
<?php 

include('footer.php');

?>
    


<!-- Modal -->    
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign Up</h1>
      </div>
      <div class="modal-body">
        <a class="buttonLinkInsideModal" data-toggle="modal" data-target="#signupAsDonor">Sign Up as Donor</a>
        <a class="buttonLinkInsideModal" href="https://docs.google.com/spreadsheets/d/16tWc9io8S8ukbe_pHcx7JdgVDVkzst3OE7rlqO14_qs/edit#gid=793542349">Sign up As Orphanage</a>          
      </div>
    </div>
  </div>
</div>    


<div class="modal fade" id="signupAsDonor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign Up As Donor</h1>
      </div>
      <div class="modal-body">
          <div id="errorBox"></div>
          <form method ="post" id="signUpAsDonor" action="registerDonor.php">
                    <label for="Reg-username">User Name</label><br/>
                    <input type="text" id="Reg-username" name="Reg-username"><br/>
            
                    <label for="Reg-email">E-mail</label><br/>
                    <input type="text" id="Reg-email" name="Reg-email"><br/>
            
                    <label for="Reg-password">Password</label><br/>
                    <input type="password" id="Reg-password" name="Reg-password"><br/>
                        
                    <label for="Reg-repassword">Confirm Password</label><br/>
                    <input type="password" id="Reg-repassword" name="Reg-repassword"><br/>
                    
                    <button type="button" onclick="checkInformationEntered()" name="DonorReg" class="btn btn-primary btn-style"> Sign Up </button>
                    <button type="button" class="btn btn-default btn-style" data-dismiss="modal">Close</button>
                    <button class="btn btn-default btn-style reset" type="reset">Reset</button>               
        </form>     
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Login As?</h1>
      </div>
      <div class="modal-body">
        <a class="buttonLinkInsideModal" data-toggle="modal" data-target="#loginAsDonor">Donor</a>
        <a class="buttonLinkInsideModal" data-toggle="modal" data-target="#loginAsOrphanage">Orphanage</a>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="loginAsDonor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign In</h1>
      </div>
      <div class="modal-body">
      	<form method="post" id="loginToSystemDonor" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    	  <label for="userName">User Name</label><br/>
     	  <input type="text" id="userName" name="userName"><br/>            
     	  <label for="password">Password</label><br/>
     	  <input type="password" id="password" name="password"><br/>            
            <a data-toggle="modal" data-target="#forgetPassword">forget password?</a><br>
     	  <button type="submit" name="loginToSystemDonor" class="btn btn-primary btn-style"> Login </button>
       </form>
      </div>
    </div>
  </div>
</div>    

<div class="modal fade" id="forgetPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign In</h1>
      </div>
      <div class="modal-body">
          <div id="errorBox"></div>
      	<form method="post" id="checkDatabase" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    	  <label for="userName">Please enter your registered Email</label><br/>
     	  <input type="text" id="emailAddress" name="emailAddress"><br/>            
     	  <button type="button" onclick="checkFieldAndDatabase()" id="retrievePass" class="btn btn-primary btn-style"> Send </button>
       </form>
      </div>
    </div>
  </div>
</div>    


<div class="modal fade" id="loginAsOrphanage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign In</h1>
      </div>
      <div class="modal-body">
      	<form method="post" id="loginToSystem" action="<?php echo $_SERVER['PHP_SELF']; ?>">            
          <label for="choose">Select Your Home</label><br/>            
          <select id="orphanageSelection" name="orphanageSelection" class="selectionPanel">
          <?php
 
            $spreadsheet_url="https://docs.google.com/spreadsheets/d/16tWc9io8S8ukbe_pHcx7JdgVDVkzst3OE7rlqO14_qs/pub?output=csv";
            if(!ini_set('default_socket_timeout',    15)) echo "<!-- unable to change socket timeout -->";

            $options ='';

            if (($handle = fopen($spreadsheet_url, "r")) !== FALSE) {
                while (($data = fgetcsv($handle,1000,",")) !== FALSE) {
                    switch($data[1]){
                        case (null):
                            echo '';
                            break;
                        case (''):
                            echo '';
                            break;
                        case("Back to CyberCare's Website"):
                            echo '';
                            break;
                        case("Home"):
                            echo '';
                            break;
                        default:    
                            $options .= '<option value="'.$data[1].'">'.$data[1].'</option>';
                            break;
                    }
                }
                echo $options;
                fclose($handle);
            }
        else
            die("Problem reading csv");

            ?>            
          </select>            
     	  <button type="submit" name="loginToSystem" class="btn btn-primary btn-style"> Login </button>
       </form>
      </div>
    </div>
  </div>
</div>    
    



<?php 
include('search.php');
?>
    
</body>
</html>