<?php

include("db_connect.php");

	class SystemUser{
        function follow($guestUserID,$personToFollow,$mysqli){
            $sql = 'INSERT INTO followedlist(Account_ID,OAccount_id) VALUES(?,?)';
            $stmt = $mysqli->prepare($sql);
            $stmt->bind_param('ii',$guestUserID,$personToFollow);
            $stmt->execute();
            echo $personToFollow;
            header('location:preview.php?name='.$personToFollow);
        }
        
        function unfollow($guestUserID,$personToUnfollow,$mysqli){
            
            $sql = 'DELETE FROM followedlist WHERE Account_ID=? AND OAccount_id=?';
            $stmt = $mysqli->prepare($sql);
            $stmt->bind_param('ii',$guestUserID,$personToUnfollow);
            $stmt->execute();

            header('location:preview.php?name='.$personToUnfollow);

        }
    }
?>