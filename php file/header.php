<!DOCTYPE html>
<html>
<head>
    <title>Home page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">    
    <link href = "homestyle.css" rel="stylesheet">     
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">    
    <script language="javascript" type="text/javascript" src="js/jquery-1.11.3.js"></script>
    <script language="javascript" type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>
    <div class="headers">
        <div class="header-bar">
            <h1>Cybercare Wish List</h1>
            <ul>                
                <li><a href="#"><img src="images/profile.png" alt="profile"/></a></li>         
                <li><a href="#"><img src="images/search.png" alt="navigation"/></a></li>     
                <li><a href="#"><img src="images/home.png" alt="home page"/></a></li>      
            </ul>
        </div>
    </div>