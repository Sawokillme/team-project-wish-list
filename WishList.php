<?php 

include("db_connect.php");

	class WishList{
            
           public $user =  null;
        
        function WishList($user){
            $this->$user = $user;
        }
        
        function deleteItem($itemID,$mysqli,$userID){
            $sql = 'DELETE FROM addeditem WHERE item_id="'.$itemID.'" AND OAccount_id="'.$userID.'";';
            $stmt = $mysqli->query($sql);
            if(mysqli_affected_rows($mysqli)>0){
                return true;
            }
            else return false;
        }
        
        function addItem($item,$quantity,$mysqli,$user){
            if(isset($item,$quantity) && $item!==null && $quantity!==null && $quantity>0 ){
            
            $sql = 'SELECT item_id FROM item WHERE item_type="'.$item.'" LIMIT 1;';
            $stmt = $mysqli->query($sql);
            while($row=$stmt->fetch_assoc()){
                $itemID=$row['item_id'];
            }            
            
			//check if the item exist
			$sql = 'SELECT * FROM addeditem WHERE item_id="'.$itemID.'" AND OAccount_id="'.$user.'";';
            $stmt = $mysqli->query($sql);
            if($stmt->num_rows > 0){
                $_SESSION['msg']='<div id="messageBox" class="message messageBox"><p>Item Exist</p></div>';
                return false;
            }
			else{
                
            $sql = 'SELECT Wish_List_id FROM wishlist WHERE OAccount_id="'.$user.'" LIMIT 1;'; 
            $stmt = $mysqli->query($sql);
            while($row=$stmt->fetch_assoc()){
                $temp=$row['Wish_List_id'];
            }    
			//check if exceed 10 item
            $sql = 'SELECT * FROM addeditem WHERE OAccount_id="'.$user.'";';
            $stmt = $mysqli->query($sql);
				if($stmt->num_rows > 9){
                $_SESSION['msg']='<div id="messageBox" class="message messageBox"><p>Cannot Add More Than 10 items</p></div>';
                    return false;
				}
				else{            
				$sql = 'INSERT INTO addeditem(item_id, Wish_list_id ,quantity ,OAccount_id) VALUES (?,?,?,?)';
				$insert_stmt2=$mysqli->prepare($sql);
				$insert_stmt2->bind_param("siii", $itemID, $temp, $quantity, $user);
				$insert_stmt2->execute();
				}              
                return true;
			}
		}
            else return false;
       
        }
        
        function updateItem($thirdItem,$quantity,$orignalItem,$mysqli,$userID){
            
            $originalID = '';
            $newItemID = '';
            
            $sql = 'SELECT item_id FROM item WHERE item_type="'.$orignalItem.'" LIMIT 1;';
            $stmt = $mysqli->query($sql);
            if($stmt->num_rows>0){
            while($row=$stmt->fetch_assoc()){
                $originalID=$row['item_id'];
            }
            }
            else return false;

            $sql = 'SELECT item_id FROM item WHERE item_type="'.$thirdItem.'" LIMIT 1;';
            $stmt = $mysqli->query($sql);
            if($stmt->num_rows>0){
            while($row=$stmt->fetch_assoc()){
                $newItemID=$row['item_id'];
            }}
            else return false;

            $sql = 'UPDATE addeditem SET item_id=?,quantity=? WHERE item_id=? AND OAccount_id=?';
            $stmt = $mysqli->prepare($sql);
            $stmt->bind_param('sisi',$newItemID,$quantity,$originalID,$userID);
            $stmt->execute();
            return true;
        }
    }

?>