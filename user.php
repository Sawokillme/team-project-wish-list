<?php

include("db_connect.php");

	class user{
		
		public $stmt = null;
		public $prep_sql = null;
		
        
		public function DonorRegister($mysqli){        
        $username = $_POST['Reg-username'];
        $email = $_POST['Reg-email'];
        $password = $_POST['Reg-password'];
        $repassword = $_POST['Reg-repassword'];
        
        if(isset($password,$username,$email,$repassword) && $password!=="" && $username!=="" && $email!=="" && $repassword!==""){
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                if($password == $repassword ){                    
                    //check if the email is used
                    $prep_sql = 'SELECT Name,email FROM account WHERE email =? LIMIT 1;';
                    $stmt = $mysqli->prepare($prep_sql);                   
                    
                    if($stmt){
                        $stmt->bind_param('s',$email);
                        $stmt->execute();
                        $stmt->store_result();
                            
                            if($stmt->num_rows == 1){
                            }
                            else{
                                $user_type = 'donor';
                                $insert_stmt = $mysqli->prepare("INSERT INTO account(Name, Password, Type_of_user, email) VALUES (?, ?, ?, ?);");
                                $insert_stmt->bind_param('ssss', $username, $password, $user_type,$email);
                                $insert_stmt->execute();
                            }
                        
                        $userID='';
                        $type='';
                        $sql='SELECT * FROM account WHERE email="'.$email.'" LIMIT 1;';
                        $stmt=$mysqli->query($sql);
                        while($row=$stmt->fetch_assoc()){
                            $userID = $row['Account_id'];                             
                            $type='donor';
                        }                        
                        $_SESSION['type']='donor';
                        $_SESSION['user']=$row['Account_id'];
                        return true;
                        header("Location:donorHomePage.php");
						}
                    return false;
					}
                else
                return false;
				}
            else
                return false;            
			}
		}
		
        //this one is no use anymore however it might be useful in future
		function OrphanageRegister($mysqli){
		$stmt=null;
        $username = $_POST['Reg-username'];
        $email = $_POST['Reg-email'];
        $password = $_POST['Reg-password'];
        $repassword = $_POST['Reg-repassword'];
        $address1= $_POST['Reg-address1'];
        $address2= $_POST['Reg-address2'];
        $postCode= $_POST['Reg-pc'];
        $state= $_POST['Reg-state'];
        $contact= $_POST['Reg-cn'];
        $personInCharge= $_POST['Reg-pic'];
        $numberOfKids= $_POST['Reg-kids'];
        $accountID='';
        
        if(isset($password,$username,$email,$repassword,$address1,$address2,$postCode,$state,$contact,$personInCharge,$numberOfKids) && $password!=="" && $username!=="" && $email!=="" && $repassword!=="" && $address1!=="" && $address2!=="" && $postCode!=="" && $state!=="" && $personInCharge!=="" && $numberOfKids!=="" ){
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                echo "valid email format"; 
                if($password == $repassword ){
                    
                    echo 'same';
                    //check if the email is used
                    $prep_sql = 'SELECT Name,email FROM account WHERE email =? LIMIT 1;';
                    $stmt = $mysqli->prepare($prep_sql);
                   
                    
                    if($stmt){
                        $stmt->bind_param('s',$email);
                        $stmt->execute();
                        $stmt->store_result();
                            
                            if($stmt->num_rows == 1){
                                echo 'error';
                            }
                            else{
                                $user_type = 'orphanage';
                                $insert_stmt = $mysqli->prepare("INSERT INTO account(Name,Contact_no,Password, Type_of_user, email) VALUES (?, ?, ?, ?, ?)");
                                $insert_stmt->bind_param("sisss", $username, $contact, $password, $user_type,$email);
                                $insert_stmt->execute();
                                
                                
                                //$sql = 'SELECT DISTINCT item_sub_category FROM item WHERE item_main_category="'.$itemMain.'";';
                                //select account_id
                                $selectStmt = 'SELECT Account_id FROM account WHERE email="'.$email.'" LIMIT 1;';
                                $stmt = $mysqli->query($selectStmt);
                                while($row=$stmt->fetch_assoc()){
                                    $accountID = $row['Account_id'];
                                }
                              
                                //enter into orphanage db
                                $insert_stmt2 = $mysqli->prepare("INSERT INTO orphanage(OAccount_id, Person_in_charge,Number_of_kids, address1, address2, PostCode, state) VALUES (?, ?, ?, ?, ?, ?, ?)");
                                $insert_stmt2->bind_param("isissis", $accountID, $personInCharge, $numberOfKids, $address1,$address2,$postCode,$state);
                                $insert_stmt2->execute();
                                
                                //create and enter into wishlist db
                                $insert_stmt3 = $mysqli->prepare("INSERT INTO wishlist(OAccount_id) VALUES (?)");
                                $insert_stmt3->bind_param("i",$accountID);
                                $insert_stmt3->execute();
                                
                                sessionStart($mysqli,$username,$password);
                                header("Location:wishList(Orphanage).php");
                            }
                        $stmt->close();
						}
                    
					}   
				}
			}
		}
        
        function LoginToSystemAsDonor($mysqli){
            $loginUserName = $_POST['userName'];
            $password = $_POST['password'];
            $sql='SELECT Account_id,Name,Password,email FROM account WHERE Name="'.$loginUserName.'" OR email="'.$loginUserName.'";';
            $stmt=$mysqli->query($sql);
            while($row=$stmt->fetch_assoc()){
                if($row['Password']==$password){
                    $_SESSION['user']=$row['Account_id'];
                    $_SESSION['type']='donor';
                    header('Location:donorHomePage.php');
                }
                else{
                }
            }
        }
        
		
		function LoginToSystem($mysqli){

		$selection = $_POST['orphanageSelection'];
            $go = '';
			$sql='';
			$stmt=null;		
			$row=null;
			$contact='';
			$contact='';
			$sqlUpdate='';
			$fileLine = array();
			$tempArray = array();

			
			$spreadsheet_url="https://docs.google.com/spreadsheets/d/16tWc9io8S8ukbe_pHcx7JdgVDVkzst3OE7rlqO14_qs/pub?output=csv";
			if(!ini_set('default_socket_timeout',    15)) echo "<!-- unable to change socket timeout -->";
			
			if (($handle = fopen($spreadsheet_url, "r")) !== FALSE) {
						while (($data = fgetcsv($handle,1000,",")) !== FALSE) {
							$fileLine[] = array('Home'=>$data[1],
												'Address1'=>$data[2],
												'Address2'=>$data[3].' '.$data[4],
												'Town'=>$data[5],
												'PostCode'=>$data[7],
												'Person_contact'=>$data[8],
												'Email'=>$data[9],
												'tel'=>$data[11],
												'NoKids'=>$data[17],
                                                'State'=>$data[6]);
						}
				fclose($handle);
			}
			else
				die("Problem reading csv");				
			
			$homeName='';
			
			if(isset($selection)&&$selection!=null){
				
				foreach($fileLine as $value){
					if($value['Home']==$selection){
						$tempArray=$value;
						break;
					}
				}
				
				$sql = 'SELECT * FROM account WHERE Name="'.$selection.'" LIMIT 1;';
				$stmt = $mysqli->query($sql);
				if($stmt->num_rows==1){
					while($row=$stmt->fetch_assoc()){
						$homeName = $row['Name'];					
					}
				$userID='';
				
				$sql = 'UPDATE account SET Contact_no=?,email=? WHERE Name=?';
				$stmt = $mysqli->prepare($sql);
				$stmt->bind_param('sss',$tempArray['tel'],$tempArray['Email'],$homeName);
				$stmt->execute();
				
				$sql = 'SELECT * FROM account WHERE Name="'.$homeName.'" LIMIT 1;';
				$stmt = $mysqli->query($sql);
				while($row = $stmt->fetch_assoc()){
					$userID=$row['Account_id'];
				}
				
				$sql = 'UPDATE orphanage SET Person_in_charge=?,Number_of_kids=?,address1=?,address2=?,PostCode=?,state=? WHERE OAccount_id=?;';
                $stmt=$mysqli->prepare($sql);
				$stmt->bind_param('sissisi',$tempArray['Person_contact'],
											$tempArray['NoKids'],
											$tempArray['Address1'],
											$tempArray['Address2'],
											$tempArray['PostCode'],
                                            $tempArray['State'],
											$userID);
				$stmt->execute();
				
                $stmt->close();
                    $_SESSION['user']=$userID;
                    $_SESSION['type']='orphanage';
                    return true;
                    //header("Location:wishList(Orphanage).php");
				}
				elseif ($stmt->num_rows<1){
					
					$accountID='';
					$user_type = 'orphanage';
                    $insert_stmt = $mysqli->prepare("INSERT INTO account(Name, Contact_no, Type_of_user, email, Profile_picture) VALUES (?, ?, ?, ?, ?)");
                    $defaultPic="uploads/blank.png";
                    $insert_stmt->bind_param("sssss", $tempArray['Home'], $tempArray['tel'], $user_type, $tempArray['Email'],$defaultPic);
                    $insert_stmt->execute();
                                
                    $selectStmt = 'SELECT Account_id FROM account WHERE Name="'. $tempArray['Home'] .'";';
                    $stmt = $mysqli->query($selectStmt);
                    while($row=$stmt->fetch_assoc()){
						$accountID = $row['Account_id'];
                    }
                              
                    //enter into orphanage db
                    $insert_stmt = $mysqli->prepare('INSERT INTO orphanage(OAccount_id,Person_in_charge,Number_of_kids,address1,address2,PostCode) VALUES(?,?,?,?,?,?)');
                    $insert_stmt->bind_param("isissi", $accountID,$tempArray['Person_contact'],$tempArray['NoKids'],$tempArray['Address1'],$tempArray['Address2'],$tempArray['PostCode']);
                    $insert_stmt->execute();
                        echo $accountID;
                    //create and enter into wishlist db
                    $insert_stmt3 = $mysqli->prepare('INSERT INTO wishlist(OAccount_id) VALUES (?)');
                    $insert_stmt3->bind_param("i",$accountID);
                    $insert_stmt3->execute();

					$stmt->close();
                    $_SESSION['user']=$accountID;
                    $_SESSION['type']='orphanage';
                    $go = true;           
                    return $go;
                    //header("Location:wishList(Orphanage).php");
				}
                else return false;
			}			
            else return false;

        }
		
	}
?> 