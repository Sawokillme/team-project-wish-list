<?php

include("db_connect.php");

	class user{
		
		public $stmt = null;
		public $prep_sql = null;
		
		function DonorRegister($mysqli){        
        $username = $_POST['Reg-username'];
        $email = $_POST['Reg-email'];
        $password = $_POST['Reg-password'];
        $repassword = $_POST['Reg-repassword'];
        
        if(isset($password,$username,$email,$repassword) && $password!=="" && $username!=="" && $email!==""
          && $repassword!==""){
            echo 'passhere';
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                echo "valid email format"; 
                if($password == $repassword ){
                    
                    echo 'same';
                    //check if the email is used
                    $prep_sql = 'SELECT Name,email FROM account WHERE email =? LIMIT 1;';
                    $stmt = $mysqli->prepare($prep_sql);                   
                    
                    if($stmt){
                        $stmt->bind_param('s',$email);
                        $stmt->execute();
                        $stmt->store_result();
                            
                            if($stmt->num_rows == 1){
                                echo 'exist';
                            }
                            else{
                                $user_type = 'donor';
                                $insert_stmt = $mysqli->prepare("INSERT INTO account(Name, Password, Type_of_user, email) VALUES (?, ?, ?, ?);");
                                $insert_stmt->bind_param('ssss', $username, $password, $user_type,$email);
                                echo 'insert';
                                $insert_stmt->execute();
                            }
                        $stmt->close();
						}
						
					}   
				}
			}
		}
		
		function OrphanageRegister($mysqli){
		$stmt=null;
        $username = $_POST['Reg-username'];
        $email = $_POST['Reg-email'];
        $password = $_POST['Reg-password'];
        $repassword = $_POST['Reg-repassword'];
        $address1= $_POST['Reg-address1'];
        $address2= $_POST['Reg-address2'];
        $postCode= $_POST['Reg-pc'];
        $state= $_POST['Reg-state'];
        $contact= $_POST['Reg-cn'];
        $personInCharge= $_POST['Reg-pic'];
        $numberOfKids= $_POST['Reg-kids'];
        $accountID='';
        
        if(isset($password,$username,$email,$repassword,$address1,$address2,$postCode,$state,$contact,$personInCharge,$numberOfKids) && $password!=="" && $username!=="" && $email!=="" && $repassword!=="" && $address1!=="" && $address2!=="" && $postCode!=="" && $state!=="" && $personInCharge!=="" && $numberOfKids!=="" ){
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                echo "valid email format"; 
                if($password == $repassword ){
                    
                    echo 'same';
                    //check if the email is used
                    $prep_sql = 'SELECT Name,email FROM account WHERE email =? LIMIT 1;';
                    $stmt = $mysqli->prepare($prep_sql);
                   
                    
                    if($stmt){
                        $stmt->bind_param('s',$email);
                        $stmt->execute();
                        $stmt->store_result();
                            
                            if($stmt->num_rows == 1){
                                echo 'error';
                            }
                            else{
                                $user_type = 'orphanage';
                                $insert_stmt = $mysqli->prepare("INSERT INTO account(Name,Contact_no,Password, Type_of_user, email) VALUES (?, ?, ?, ?, ?)");
                                $insert_stmt->bind_param("sisss", $username, $contact, $password, $user_type,$email);
                                $insert_stmt->execute();
                                
                                
                                //$sql = 'SELECT DISTINCT item_sub_category FROM item WHERE item_main_category="'.$itemMain.'";';
                                //select account_id
                                $selectStmt = 'SELECT Account_id FROM account WHERE email="'.$email.'" LIMIT 1;';
                                $stmt = $mysqli->query($selectStmt);
                                while($row=$stmt->fetch_assoc()){
                                    $accountID = $row['Account_id'];
                                }
                              
                                //enter into orphanage db
                                $insert_stmt2 = $mysqli->prepare("INSERT INTO orphanage(OAccount_id, Person_in_charge,Number_of_kids, address1, address2, PostCode, state) VALUES (?, ?, ?, ?, ?, ?, ?)");
                                $insert_stmt2->bind_param("isissis", $accountID, $personInCharge, $numberOfKids, $address1,$address2,$postCode,$state);
                                $insert_stmt2->execute();
                                
                                //create and enter into wishlist db
                                $insert_stmt3 = $mysqli->prepare("INSERT INTO wishlist(OAccount_id) VALUES (?)");
                                $insert_stmt3->bind_param("i",$accountID);
                                $insert_stmt3->execute();
                                
                                sessionStart($mysqli,$username,$password);
                                header("Location:wishList(Orphanage).php");
                            }
                        $stmt->close();
						}
                    
					}   
				}
			}
		}
		
		function LoginToSystem($mysqli){
        $loginUserName = $_POST['userName'];
        $password = $_POST['password'];
    
        if(isset($loginUserName,$password) && $loginUserName!=='' && $password!==''){
            $prep_sql = 'SELECT * FROM account WHERE Name =? AND Password =? LIMIT 1;';
            $stmt = $mysqli->prepare($prep_sql);
                if($stmt){
                    $stmt->bind_param('ss',$loginUserName,$password);
                    $stmt->execute();
                    $stmt->store_result();
                    if($stmt->num_rows == 1){    
                            $this->sessionStart($mysqli,$loginUserName,$password);
                            header("Location:wishList(Orphanage).php");                            
                        }
                                
                            }
                    else{
                        $prep_sql = 'SELECT * FROM account WHERE email =? AND Password =? LIMIT 1;';
                        $stmt = $mysqli->prepare($prep_sql);
                        if($stmt){
                            $stmt->bind_param('ss',$loginUserName,$password);
                            $result = $stmt->execute();
                            $stmt->store_result();
                            if($stmt->num_rows == 1){
                                sessionStart($mysqli,$loginUserName,$password);
                                header("Location:wishList(Orphanage).php");
                            }
                            else
                                $stmt->close();
                    }
                }
            }
        }
		
		function sessionStart($mysqli,$loginUserName,$password){
        $prep_sql = 'SELECT Account_id FROM account WHERE Name =? AND Password =? LIMIT 1;';
        $stmt = $mysqli->prepare($prep_sql);
            if($stmt){
                $stmt->bind_param('ss',$loginUserName,$password);
                $stmt->execute();
                $stmt->bind_result($user);
                    if($stmt->fetch()){
                        $_SESSION['user']=$user;
                            echo $_SESSION['user'];
                    }
            }
		$stmt->close();
		}
		
	}
?> 