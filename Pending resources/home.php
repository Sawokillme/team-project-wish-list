<?php
//include_once 'db_connect.php';

session_start();
include("db_connect.php");

include("user.php");

$publicUser = new user();

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['DonorReg']))
        $publicUser->DonorRegister($mysqli);
    elseif(isset($_POST['OrpReg']))
        $publicUser->OrphanageRegister($mysqli);
    elseif(isset($_POST['loginToSystem']))
        $publicUser->LoginToSystem($mysqli);
}

include_once 'header.php';

?>
<div class="banners">
       <div class="adSection">
           <h1 class="ad">Sign Up Now For FREE!</h1>
           <h2 class="firstQuote"><em>&quot;one has never become poor by giving.&quot;</em></h2>
           <h2 class="author"><em>-Anne Frank, diary of Anne Frank</em></h2>
           <a class="buttonLink" data-toggle="modal" data-target="#signup">Sign Up</a>
           <a class="buttonLink" data-toggle="modal" data-target="#login">Sign In</a>
       </div>
    </div>
    
    <div class="miniTitleBar">
        <h1>Who to donate?</h1>
        <h2>Donate To Them</h2>
    </div>
    
    <div class="searchBar">
        <form class="search">
                <input class="searchInput" type="text" placeholder="Type here to search">
                <button type="submit">search</button>
        </form>
    </div>
    
    <div class="context-container">
        <div class="Incontainer">
            <div class="context">                
                <div class="briefInformation">
                    <table> 
                        <caption><a href="wishList.html">My Home</a></caption>
                        <thead>
                        <tr>
                            <td class="tableHead">Category</td>
                            <td class="tableHead">SubCategory</td>
                            <td class="tableHead">Item</td>
                            <td class="tableHead">Qtn.</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="firstCate">Category</td>
                            <td class="secondCate">Document tray and desk equipment</td>
                            <td class="thirdCate">Item</td>
                            <td class="quantity">100</td>
                        </tr>
                        <tr>
                            <td class="firstCate">Stationery</td>
                            <td class="secondCate">Crafting and coloring</td>
                            <td class="thirdCate">Whiteboard Marker pen ( Green )</td>
                            <td class="quantity">100</td>
                        </tr>
                        <tr>
                            <td class="firstCate">Category</td>
                            <td class="secondCate">SubCategory</td>
                            <td class="thirdCate">Item</td>
                            <td class="quantity">Qtn.</td>
                        </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4"><a class="toWishList" href="wishList.html">See More</a></td>
                            </tr>
                           </tfoot>
                    </table>
                </div>
                
                <img class="profile-picture" src="images/Black-pictures-dark-wallpapers-hd-photos-dark-wallpaper-16.jpg">                              
            </div>        
        </div>
    </div>
    
    
    <div class="context-container">
        <div class="Incontainer">
            <div class="context">                
                <div class="briefInformation">
                    <table> 
                        <caption><a href="wishList.html">My Home</a></caption>
                        <thead>
                        <tr>
                            <td class="tableHead">Category</td>
                            <td class="tableHead">SubCategory</td>
                            <td class="tableHead">Item</td>
                            <td class="tableHead">Qtn.</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="firstCate">Category</td>
                            <td class="secondCate">Document tray and desk equipment</td>
                            <td class="thirdCate">Item</td>
                            <td class="quantity">100</td>
                        </tr>
                        <tr>
                            <td class="firstCate">Stationery</td>
                            <td class="secondCate">Crafting and coloring</td>
                            <td class="thirdCate">Whiteboard Marker pen ( Green )</td>
                            <td class="quantity">100</td>
                        </tr>
                        <tr>
                            <td class="firstCate">Category</td>
                            <td class="secondCate">SubCategory</td>
                            <td class="thirdCate">Item</td>
                            <td class="quantity">100</td>
                        </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4"><a class="toWishList" href="wishList.html">See More</a></td>
                            </tr>
                           </tfoot>
                    </table>
                </div>
                
                <img class="profile-picture" src="images/Black-pictures-dark-wallpapers-hd-photos-dark-wallpaper-16.jpg">                              
            </div>        
        </div>
    </div>
    
    <footer>
        <div class="footer footer-bar footer-detail">
            <div class="location">
                <h3>location</h3>
                <p>19. Jln PBS 14/3 Tamam perisdustrian Bukit Serdang. 43300 Seri kembangan</p>
            </div>
            <div class="email col-xs-3">
                <h3>Email </h3>
                <p>mok@cybercare.org.my </p>
                <p>elena@cybercare.org.my</p>
            </div>
            <div class="contact col-xs-3">
                <h3>Contact Number</h3>
            </div>
        </div>
        <div class="copyFooter">
         <div class="copyRight">&copy; 2015 CyberCare Youth Organization</div>
        </div>    
    </footer>
    
    


<!-- Modal -->    

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign In</h1>
      </div>
      <div class="modal-body">
      	<form method="post" id="loginToSystem" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    	  <label for="userName">User Name</label><br/>
     	  <input type="text" id="userName" name="userName"><br/>            
     	  <label for="password">Password</label><br/>
     	  <input type="password" id="password" name="password"><br/>
            
          <label for="choose">Select Your Home</label><br/>
            
            
          <select id="orphanageSelection" name="orphanageSelection" class="selectionPanel">
          <?php
 
            $spreadsheet_url="https://docs.google.com/spreadsheets/d/16tWc9io8S8ukbe_pHcx7JdgVDVkzst3OE7rlqO14_qs/pub?output=csv";
if(!ini_set('default_socket_timeout',    15)) echo "<!-- unable to change socket timeout -->";

$options ='';

if (($handle = fopen($spreadsheet_url, "r")) !== FALSE) {
    while (($data = fgetcsv($handle,1000,",")) !== FALSE) {
            switch($data[1]){
                case (null):
                    echo '';
                    break;
                case (''):
                    echo '';
                    break;
                case("Back to CyberCare's Website"):
                    echo '';
                    break;
                case("Home"):
                    echo '';
                    break;
                default:    
                    $options .= '<option value="'.$data[1].'">'.$data[1].'</option>';
                    break;
            }
        }
    echo $options;
    fclose($handle);
    }
    else
        die("Problem reading csv");

            ?>            
          </select>
            
     	  <button type="submit" name="loginToSystem" class="btn btn-primary btn-style"> Login </button>
       </form>
      </div>
    </div>
  </div>
</div>    

    
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign Up</h1>
      </div>
      <div class="modal-body">
        <button class="buttonLinkInsideModal" data-toggle="modal" data-target="#signupAsDonor">Sign Up as Donor</button>
        <button class="buttonLinkInsideModal" data-toggle="modal" data-target="#signupAsOrphanage">Sign up As Orphanage</button>          
      </div>
    </div>
  </div>
</div>    

<div class="modal fade" id="signupAsDonor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign Up As Donor</h1>
      </div>
      <div class="modal-body">
          <form method ="post" id="signUpAsDonor" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <label for="Reg-username">User Name</label><br/>
                    <input type="text" id="Reg-username" name="Reg-username"><br/>
            
                    <label for="Reg-email">E-mail</label><br/>
                    <input type="text" id="Reg-email" name="Reg-email"><br/>
            
                    <label for="Reg-password">Password</label><br/>
                    <input type="password" id="Reg-password" name="Reg-password"><br/>
                        
                    <label for="Reg-repassword">Confirm Password</label><br/>
                    <input type="password" id="Reg-repassword" name="Reg-repassword"><br/>
                    
                    <button type="submit" name="DonorReg" class="btn btn-primary btn-style"> Sign Up </button>
                    <button type="button" class="btn btn-default btn-style" data-dismiss="modal">Close</button>
                    <button class="btn btn-default btn-style reset" type="reset">Reset</button>               
        </form>     
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="signupAsOrphanage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel">Sign Up As Orphanage</h1>
      </div>
      <div class="modal-body">
          <form method ="post" id="signUpAsOrphanage" action="<?php echo $_SERVER['PHP_SELF']; ?>">
              <fieldset>
                  <legend>Login information</legend>
                    <label for="Reg-username">User Name</label><br/>
                    <input type="text" id="Reg-username" name="Reg-username"><br/>
            
                    <label for="Reg-email">E-mail</label><br/>
                    <input type="text" id="Reg-email" name="Reg-email"><br/>
            
                    <label for="Reg-password">Password</label><br/>
                    <input type="password" id="Reg-password" name="Reg-password"><br/>
                        
                    <label for="Reg-repassword">Confirm Password</label><br/>
                    <input type="password" id="Reg-repassword" name="Reg-repassword"><br/>
              </fieldset>
              <fieldset>
                  <legend>Personal information</legend>
                    <label for="Reg-repassword">Address Line 1</label><br/>
                    <input type="text" id="Reg-address1" name="Reg-address1"><br/>
              
                    <label for="Reg-repassword">Address Line 2</label><br/>
                    <input type="text" id="Reg-address2" name="Reg-address2"><br/>
              
                    <label for="Reg-repassword">Post Code</label><br/>
                    <input type="text" id="Reg-pc" name="Reg-pc"><br/>
              
                    <label for="Reg-repassword">State</label><br/>
                    <input type="text" id="Reg-state" name="Reg-state"><br/>
              
                    <label for="Reg-repassword">Person in charge</label><br/>
                    <input type="text" id="Reg-pic" name="Reg-pic"><br/>
              
                    <label for="Reg-repassword">Number of kids</label><br/>
                    <input type="text" id="Reg-kids" name="Reg-kids"><br/>
              
                    <label for="Reg-repassword">Contact Number</label><br/>
                    <input type="text" id="Reg-cn" name="Reg-cn"><br/>
                </fieldset>
                    <button type="submit" name="OrpReg" class="btn btn-primary btn-style"> Sign Up </button>
                    <button type="button" class="btn btn-default btn-style" data-dismiss="modal">Close</button>
                    <button class="btn btn-default btn-style reset" type="reset">Reset</button>               
        </form>     
      </div>
    </div>
  </div>
</div>
    
</body>
</html>