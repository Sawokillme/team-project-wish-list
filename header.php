<?php 

if(isset($_SESSION['type']))
$type=$_SESSION['type'];

?>

<!DOCTYPE html>
<html>
<head>
    <title>Home page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">    
    <link href = "css/homestyle.css" rel="stylesheet">     
    <link href = "css/wishListStyle.css" rel="stylesheet">     
    <link href = "css/wishListComponent.css" rel="stylesheet">  
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">    
    <script language="javascript" type="text/javascript" src="js/jquery-1.11.3.js"></script>
    <script language="javascript" type="text/javascript" src="js/bootstrap.js"></script>
    <script language="javascript" type="text/javascript" src="js/main.js"></script>
</head>
<body>
    <div class="headers">
        <div class="header-bar">
            <h1>Cybercare Wish List</h1>
            <ul>                
                <?php
                
                if(isset($_SESSION['user'])){
                    $logout='<li><a href="logout.php"><img src="images/profile.png" alt="profile"/></a></li>';
                    echo $logout;
                }
                
                ?>                 
                <li><a data-toggle="modal" data-target="#searchItem"><img src="images/search.png" alt="navigation"/></a></li>     
                
                <?php
                
                if(isset($_SESSION['user'])){
                    if($type=='orphanage')
                        $homeForUser='<li><a href="wishList(orphanage).php"><img src="images/home.png" alt="home page"/></a></li> ';
                    else
                        $homeForUser='<li><a href="donorHomePage.php"><img src="images/home.png" alt="home page"/></a></li> ';
                    echo $homeForUser;
                }
                else{
                    echo '<li><a href="home.php"><img src="images/home.png" alt="home page"/></a></li>';
                }
                
                ?>
                   
            </ul>
        </div>
    </div>