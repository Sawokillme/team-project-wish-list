<?php 
session_start();
include('../user.php');
include('../WishList.php');
            

class TestUser extends PHPUnit_Framework_TestCase{
    
        protected $testObject;
        protected $mysqli;
        protected function setUp(){
            $this->testWishListObject = new WishList("44");
            $this->testObject = new user();
            $servername = 'localhost';
            $username = 'root';
            $password = '';
            $database = 'wishlist';

            $this->mysqli = new mysqli($servername, $username, $password, $database);
            $this->mysqli->autocommit(FALSE);
        }
    
    /**
    *@dataProvider providerForDonorRegister
    */
    public function testDonorRegister($name,$email,$password,$repassword,$expectedResult){
        $_POST['Reg-username']=$name;
        $_POST['Reg-email']=$email;
        $_POST['Reg-password']=$password;
        $_POST['Reg-repassword']=$repassword;   
        $result = $this->testObject->DonorRegister($this->mysqli);
        $this->assertSame($expectedResult,$result);        
        $this->mysqli->rollback();
    }
        
    public function providerForDonorRegister(){
        return array(
            array("louis Loh","azp@hotmail.com","testPass","testPass",true),
            array("louis Loh","azp@hotmail","testPass","testPass",false),
            array("louis Loh","azp@hotmail.com","testPass","test",false)
        );
    }
    
    
    
    /**
    *@dataProvider providerForOrphanageLogin
    */
    public function testOrphanageLogin($orphanageName,$expectedResult){
        $_POST['orphanageSelection'] = $orphanageName;
        $result = $this->testObject->LoginToSystem($this->mysqli);
        $this->assertSame($expectedResult,$result);        
        $this->mysqli->rollback();
    }
    
    public function providerForOrphanageLogin(){
        return array(
            array("Pert. Kebajikan Anak Yatim Islam Mersing",true),
            array("newOrphanage",true), // note that this data is to test if new orphanage is entered
            array("",false),
        );
    }
    
    /**
    *@dataProvider providerFordeleteitem
    */
    public function testDeleteItem($itemID,$OaccountID,$expectedResult){
        $result = $this->testWishListObject->deleteItem($itemID,$this->mysqli,$OaccountID);
        $this->assertSame($expectedResult,$result);        
        $this->mysqli->rollback();
    }
    
    public function providerFordeleteitem(){
        return array(
            array("1","44",true),
            array("0","33",false), // note that this data is to test if new orphanage is entered
            array("1","47",true),
        );
    }
    
    
    /**
    *@dataProvider providerForAddItem
    */    
    public function testAddItem($item,$quantity,$user,$expectedResult){
        $result = $this->testWishListObject->addItem($item,$quantity,$this->mysqli,$user);
        $this->assertSame($expectedResult,$result);        
        $this->mysqli->rollback();
    }
    
    public function providerForAddItem(){
        return array(
            array("White loaves","44","41",true),
            array("White loaves","2","32",true), // note that this data is to test if new orphanage is entered
            array("White loaves","-1","",false),
        );
    }
    
    
    /**
    *@dataProvider providerForUpdate
    */    
    public function testUpdateItem($itemToChange,$quantity,$original,$user,$expectedResult){
        $result = $this->testWishListObject->updateItem($itemToChange,$quantity,$original,$this->mysqli,$user);
        $this->assertSame($expectedResult,$result);        
        $this->mysqli->rollback();
    }
    
    public function providerForUpdate(){
        return array(
            array("Burgers","1","White loaves","47",true),
            array("Apple","1","White loaves","47",true),
            array("Bur","1","White loaves","47",false),
        );
    }
    
    
}

?>