<div class="modal fade" id="searchItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Search</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="searchItem">
            <label>Category:</label>
            <select id="selectedCategory">
                <option ></option>
                <option id="ItemSelection" value="item">Item</option>
                <option id="OrphanageSelection" value="orphanage">Orphanage</option>
            </select>
            <label>Keyword: </label>
            <input id="keyword" type="text" list="searchDatabase">          
            <div id="contentDisplay">     
              <label>Result</label>
            </div>
          <button type="button" name="addItem" class="btn btn-primary searchButton"> Search </button>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    $(".searchButton").click(function() {
        var selection = document.getElementById('selectedCategory').value;
        var keyword = document.getElementById('keyword').value;
        console.log(selection);
        console.log(keyword);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                  document.getElementById("contentDisplay").innerHTML=xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "searchDatabase.php?selection=" + selection +"&keyword="+ keyword, true);
        xmlhttp.send();
    });
    
    document.getElementById('selectedCategory').addEventListener('change', function () {
        var getValue = document.getElementById('selectedCategory').value;
        if(getValue == 'item'){
        console.log(getValue);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("keyword").innerHTML= xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "searchDatalist.php", true);
        xmlhttp.send();
        }
        else
            document.getElementById("keyword").innerHTML=null;
    });


</script>