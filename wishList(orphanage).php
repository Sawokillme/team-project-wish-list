

<?php 
    session_start();
    $user = $_SESSION['user'];

    $_SESSION['type']='orphanage';
    include("db_connect.php");
    include("orphanage.php");
    require('WishList.php');

    $orphanage = new Orphanage($user,$mysqli);
    $wishList = new WishList($user);
    $_SESSION['wishListObject']=$wishList;
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(isset($_POST['addItem']))
            $orphanage->addItem($mysqli,$user);
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Wish List page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">    
    <link href = "css/homestyle.css" rel="stylesheet">  
    <link href = "css/wishListStyle.css" rel="stylesheet">     
    <link href = "css/wishListComponent.css" rel="stylesheet">  
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">    
    <script language="javascript" type="text/javascript" src="js/jquery-1.11.3.js"></script>
    <script language="javascript" type="text/javascript" src="js/bootstrap.js"></script>    
</head>
<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        
    
    <div class="headers">
        <div class="header-bar">
            <h1>Cybercare Wish List</h1>
            <ul>                
                <?php
                
                if(isset($_SESSION['user'])){
                    $logout='<li><a href="logout.php"><img src="images/profile.png" alt="profile"/></a></li>';
                    echo $logout;
                }
                
                ?>                 
                <li><a data-toggle="modal" data-target="#searchItem"><img src="images/search.png" alt="navigation"/></a></li>     
                
                <?php
                
                if(isset($_SESSION['user'])){
                    $homeForUser='<li><a href="wishList(orphanage).php"><img src="images/home.png" alt="home page"/></a></li> ';
                    echo $homeForUser;
                }
                else{
                    echo '<li><a href="home.php"><img src="images/home.png" alt="home page"/></a></li>';
                }
                
                ?>
                   
            </ul>
        </div>
    </div>

    
    <div class="wishListContainer">
        <div class="profile">
            <div class="OrphanageName">
                <h1>
            <?php
                $sql = 'SELECT Name FROM Account WHERE Account_id="'.$user.'" LIMIT 1;';
                $stmt=$mysqli->query($sql);
                while($row=$stmt->fetch_assoc()){
                    echo $row['Name'];
                }
            ?>
                </h2>
            </div>
            <div class="sidebar">
                <a data-toggle="modal" data-target="#changePicture"><img class="profilepicture"<?php
                            $pic='';                                             
                        $sql='SELECT Profile_picture FROM account WHERE Account_id="'.$user.'" LIMIT 1;';
                        $stmt=$mysqli->query($sql);
                        while($row=$stmt->fetch_assoc()){
                            $pic=$row['Profile_picture'];
                        }
                        echo 'src="'.$pic.'"';
                                                                         ?></a>
                <ul>
                    <li><a href="https://docs.google.com/spreadsheets/d/16tWc9io8S8ukbe_pHcx7JdgVDVkzst3OE7rlqO14_qs/edit#gid=793542349">Edit profile</a></li>
                    <li><a data-toggle="modal" data-target="#addItemModal">Add Item</a></li>
                </ul>
            </div>
            <div class="profileAndWishList">                
                <div class="wishList">
                    <table> 
                        <caption>Wish List</caption>
                        <thead>
                        <tr>
                            <td class="tableHead">Category</td>
                            <td class="tableHead">SubCategory</td>
                            <td class="tableHead">Item</td>
                            <td class="tableHead" colspan="3">Qtn.</td>
                        </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $itemMainCate='';
                            $itemSubCate='';
                            $itemName='';
                            $output='';
                            $result=null;
                            $sql='';
                            $stmt = null;
                            $sql2='';
                            $stmt2=null;
                            $itemID='';

                            $sql = 'SELECT * FROM addeditem WHERE OAccount_id="'.$user.'";';
                            $stmt = $mysqli->query($sql);
                            while($row=$stmt->fetch_assoc()){
                                $itemID=$row['item_id'];
                                $sql2 = 'SELECT * FROM item WHERE item_id="'.$row['item_id'].'";';
                                $stmt2 = $mysqli->query($sql2);
                                while($result=$stmt2->fetch_assoc()){
                                    $itemMainCate = $result['item_main_category'];
                                    $itemSubCate = $result['item_sub_category'];
                                    $itemName = $result['item_type'];
                                }
                                $storedQuantity=$row['quantity'];
                                $values = array("Main"=>$itemMainCate,
                                                "Sub"=>$itemSubCate,
                                                "item"=>$itemName,
                                                "Quantity"=>$storedQuantity);
                                $output.=<<<HTML
<tr id="{$itemID}">
<td class="firstCate">{$itemMainCate}</td>
<td class="secondCate">{$itemSubCate}</td>
<td class="thirdCate">{$itemName}</td>
<td class="quantity">{$storedQuantity}</td>
<td class="update"><button class="buttonUpdate" id="{$itemID}" type="submit" value="{$values['Main']}_{$values['Sub']}_{$values['item']}_{$values['Quantity']}" data-toggle="modal" data-target="#updateItemModal">Update</button></td>
<td class="delete"><button class="buttonDelete" type="submit" value="{$itemID}">Delete</button></td>
</tr>
HTML;
                            }
                            echo $output;
                            ?> 

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6">Maximum 10 item</td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="fb-share-button" data-href="preview.php?name=<?php echo $user ?>" data-layout="button_count"></div>
                </div>
            </div>    
            
            <div class="followerPanel">
                <h1>Following</h1>
            
            <?php 
            $followingList=array();

            $sql = 'SELECT OAccount_id FROM followedlist WHERE Account_ID="'.$user.'";';
            $stmt = $mysqli->query($sql);
        if($stmt->num_rows>0){
            while($row=$stmt->fetch_assoc()){
                $followingList[]=$row['OAccount_id'];
            }

            foreach($followingList as $value){
                $WholeContent = '';
                $DisplayItem='';
                $itemList = array();
                $quantity = array();
                $pic=array();
                $name='';
                
                $sql = 'SELECT Name,Profile_picture FROM account WHERE Account_id="'.$value.'"LIMIT 1;';
                $stmt=$mysqli->query($sql);
                while($row=$stmt->fetch_assoc()){
                    $name=$row['Name'];
                    $pic=$row['Profile_picture'];
                }
                
                $sql = 'SELECT * FROM addeditem WHERE OAccount_id="'.$value.'";';
                $stmt = $mysqli->query($sql);
                while($row=$stmt->fetch_assoc()){
                    $itemList[] = $row['item_id'];
                    $quantity[] = $row['quantity'];
                    
                    $sql2 = 'SELECT * FROM item WHERE item_id="'.$row['item_id'].'"LIMIT 1;';
                    $stmt2 = $mysqli->query($sql2);
                    while($row2=$stmt2->fetch_assoc()){
                        $DisplayItem.=<<<HTML
<tr>
<td class="Column1">{$row2['item_main_category']}</td>
<td class="Column2">{$row2['item_sub_category']}</td>
<td class="Column3">{$row2['item_type']}</td>
<td class="Column4">{$row['quantity']}</td>
</tr>
HTML;
                    }              
                }
                
                $WholeContent=<<<HTML
<div class="FollowerContainer">                
<div class="table">
<table> 
<caption><a href="preview.php?name={$value}">{$name}</a></caption>
<thead>
<tr>
<td class="tableHead">Category</td>
<td class="tableHead">SubCategory</td>
<td class="tableHead">Item</td>
<td class="tableHead">Qtn.</td>
</tr>
</thead>
<tbody>
{$DisplayItem}
</tbody>
<tfoot>
<tr>
<td colspan="4"><a class="toWishList" href="preview.php?name={$value}">See More</a></td>
</tr>
</tfoot>
</table>
</div>                
<img class="picture" src="{$pic}">                         
</div>                       
HTML;
            echo $WholeContent;    
            }
        }
else
    echo '<div class="FollowerContainer"><h2>Not following anyone =( </h2></div>';
            ?>
</div>
            
     <footer>
        <div class="footer footer-bar footer-detail">
            <div class="location">
                <h3>location</h3>
                <p>19. Jln PBS 14/3 Tamam perisdustrian Bukit Serdang. 43300 Seri kembangan</p>
            </div>
            <div class="email col-xs-3">
                <h3>Email </h3>
                <p>mok@cybercare.org.my </p>
                <p>elena@cybercare.org.my</p>
            </div>
            <div class="contact col-xs-3">
                <h3>Contact Number</h3>
            </div>
        </div>
            <div class="copyFooter">
                <div class="copyRight">&copy; 2015 CyberCare Youth Organization</div>
             </div>    
    </footer>
            
            
            
            <?php
include('search.php');
?>
            
            
                        
<div class="modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Item</h4>
      </div>
      <div class="modal-body">
        <div id="errorBox"></div>
        <form method="post" id="addItem" action= "addItem.php">
            <fieldset>
                <legend>Item</legend>
                <input type='text' list='1stCategory' class="MainCategory" name="1stCaregory" id="1stCaregory">
                    <datalist id='1stCategory'>
                        <?php                   
        $selection = '';
                            $prep_sql = 'SELECT DISTINCT item_main_category FROM item;';
                            $stmt = $mysqli->query($prep_sql);
                                while($row = $stmt->fetch_assoc()){
                                    $selection .= '<option label="'.$row['item_main_category'].'" value="'.$row['item_main_category'].'">';
                                }
                            echo $selection;
                        ?>
                        
                    </datalist>
                
                <input type='text' list='2ndCategory' name="2ndCaregory" id="2ndCaregory">
                    <datalist id='2ndCategory' name='2ndCate'>
                        
                    </datalist>
                <input type='text' list='3rdCategory' name="3rdCaregory" id="3rdCaregory">
                    <datalist id='3rdCategory'>
                        
                    </datalist>
            </fieldset>
            <fieldset>
                <legend>Quantity</legend>
                <input type="number" name="quantity" id="quantity">
            </fieldset>
            <button type="button" onclick="checkAddItem()" name="additem2" class="btn btn-primary"> Add Item </button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="reset" class="btn btn-default reset">Reset</button>
          </form>
      </div>
    </div>
  </div>
</div>
            
<div class="modal fade" id="updateItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Item</h4>
      </div>
      <div class="modal-body">
        <div id="errorBox"></div>
        <form method="post" id="updateItem" action="wishList(orphanage).php">
            <fieldset>
                <legend>Item</legend>
                <input type='text' list='1stCategory' class="MainCategory" name="1stCaregory" id="1stCaregoryupdate">
                    <datalist id='1stCategoryupdate'>
                        <?php                   
        $selection = '';
                            $prep_sql = 'SELECT DISTINCT item_main_category FROM item;';
                            $stmt = $mysqli->query($prep_sql);
                                while($row = $stmt->fetch_assoc()){
                                    $selection .= '<option label="'.$row['item_main_category'].'" value="'.$row['item_main_category'].'">';
                                }
                            echo $selection;
                        ?>
                    </datalist>
                
                <input type='text' list='2ndCategoryupdate' name="3rdCategoryupdate" id="2ndCaregoryupdate">
                    <datalist id='2ndCategoryupdate'>
                    </datalist>
                <input type='text' list='3rdCategoryupdate' name="3rdCategoryupdate" id="3rdCaregoryupdate">
                    <datalist id='3rdCategoryupdate'>
                    </datalist>
            </fieldset>
            <fieldset>
                <legend>Quantity</legend>
                <input type="number" name="quantity" id="quantityupdate">
            </fieldset>
            <script type="text/javascript">
            </script>
            <button type="button" onclick="checkUpdateItem()" id="updateItemButton" name="updateItem" class="btn btn-primary button-update"> Update Item </button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="reset" class="btn btn-default reset">Reset</button>
          </form>
      </div>
    </div>
  </div>
</div>
            
<div class="modal fade" id="changePicture" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Profile Picture</h4>
      </div>
      <div class="modal-body">
        <div id="errorBox"></div>
          <form action="upload.php" method="post" enctype="multipart/form-data">
            Select image to upload:
                <input type="file" name="fileToUpload" id="fileToUpload">
              <input type="submit" value="Upload Image" name="submit">
          </form>
      </div>
    </div>
  </div>
</div>
            
            <script type="text/javascript">   
                var originalItem ="";
                document.getElementById('1stCaregory').addEventListener('input', function () {
                    var getValue = document.getElementById('1stCaregory').value;
                    console.log(getValue);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            document.getElementById("2ndCategory").innerHTML= xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "printOption.php?item_main_category=" + getValue, true);
                    xmlhttp.send();
                });
                
                document.getElementById('2ndCaregory').addEventListener('input', function () {
                    var getValue = document.getElementById('2ndCaregory').value;
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            document.getElementById("3rdCategory").innerHTML= xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "printOptionForUpdate.php?item_sub_category=" + getValue, true);
                    xmlhttp.send();
                });
                
            
                
                
                    $(".buttonDelete").click(function() {
                        console.log(this.value);
                        var className = this.value;
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                           var parent = document.getElementById(className);
                            while(parent.hasChildNodes()){
                                parent.removeChild(parent.firstChild);
                            }
                        }
                    }
                        xmlhttp.open("GET", "deletion.php?item_id=" + this.value, true);
                        xmlhttp.send();
                    });
                
                var seperated="";
                            
                
                document.getElementById('1stCaregory').addEventListener('input', function () {
                    var getValue = document.getElementById('1stCaregory').value;
                    document.getElementById('2ndCaregory').value="";
                    document.getElementById('3rdCaregory').value="";
                    document.getElementById('1stCaregory').value = getValue;
                    console.log(getValue);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            document.getElementById("2ndCategory").innerHTML= xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "printOption.php?item_main_category=" + getValue, true);
                    xmlhttp.send();
                });
                
                                
                document.getElementById('2ndCaregory').addEventListener('input', function () {
                    var getValue = document.getElementById('2ndCaregory').value;
                    var getFirstValue = document.getElementById('1stCaregory').value;
                    document.getElementById('2ndCaregory').value = getValue;
                    document.getElementById('3rdCaregory').value="";
                    console.log(getValue);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            document.getElementById("3rdCategoryupdate").innerHTML= xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "printOptionForUpdate.php?item_sub_category=" + getValue, true);
                    xmlhttp.send();
                });
                
                
                
                document.getElementById('1stCaregoryupdate').addEventListener('input', function () {
                    var getValue = document.getElementById('1stCaregoryupdate').value;
                    document.getElementById('2ndCaregoryupdate').value="";
                    document.getElementById('3rdCaregoryupdate').value="";
                    document.getElementById('1stCaregoryupdate').value = getValue;
                    console.log(getValue);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            document.getElementById("2ndCategoryupdate").innerHTML= xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "printOption.php?item_main_category=" + getValue, true);
                    xmlhttp.send();
                });
                
                
                
                
                                
                document.getElementById('2ndCaregoryupdate').addEventListener('input', function () {
                    var getValue = document.getElementById('2ndCaregoryupdate').value;
                    var getFirstValue = document.getElementById('1stCaregoryupdate').value;
                    document.getElementById('2ndCaregoryupdate').value = getValue;
                    document.getElementById('3rdCaregoryupdate').value="";
                    console.log(getValue);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            document.getElementById("3rdCategoryupdate").innerHTML= xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "printOptionForUpdate.php?item_sub_category=" + getValue, true);
                    xmlhttp.send();
                });
                
                
                
$(".buttonUpdate").click(function(){                       
    var getValue2 = this.value;                                
    console.log(getValue2);
    var seperated = getValue2.split('_');
    console.log(seperated);
    var find = document.getElementById("1stCaregory");
                       
//change id
    document.getElementById("1stCaregoryupdate").setAttribute("value",'');
    document.getElementById("2ndCaregoryupdate").setAttribute("value",'');
    document.getElementById("3rdCaregoryupdate").setAttribute("value",'');
    document.getElementById("quantityupdate").setAttribute("value",'');
    document.getElementById("1stCaregoryupdate").value = seperated[0];
    document.getElementById("2ndCaregoryupdate").value = seperated[1];
    document.getElementById("3rdCaregoryupdate").value = seperated[2];
    document.getElementById("quantityupdate").value = seperated[3];
    originalItem = document.getElementById("3rdCaregoryupdate").value;
    console.log(originalItem);
});
                
                
                document.getElementById('2ndCaregoryupdate').addEventListener('input', function () {
                    var getFirstValue = document.getElementById('1stCaregoryupdate').value;
                    console.log(getFirstValue);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            document.getElementById("2ndCategoryupdate").innerHTML= xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "printOption.php?item_main_category=" + getFirstValue, true);
                    xmlhttp.send();
                });
                        
                
                document.getElementById('3rdCaregoryupdate').addEventListener('input', function () {
                    var getFirstValue = document.getElementById('2ndCaregoryupdate').value;
                    console.log(getFirstValue);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            document.getElementById("3rdCategoryupdate").innerHTML= xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "printOptionForUpdate.php?item_sub_category=" + getFirstValue, true);
                    xmlhttp.send();
                });
                            
                 
                
        </script>
                    <script language="javascript" type="text/javascript" src="js/main.js"></script>    
</body>